package page;

import lombok.Getter;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


@Getter
public class HomePage {
    WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    private String URL = "http://automationpractice.com/index.php";
    private String title = "My Store";

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/a")
    private WebElement woman;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[2]/a")
    private WebElement dress;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[3]/a")
    private WebElement tShort;

    @FindBy(xpath = "//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a")
    private WebElement cart;

   @FindBy(xpath = "//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")
    private WebElement login;

    @FindBy(xpath = "//*[@id=\"contact-link\"]/a")
    private WebElement contactUs;

    @FindBy(xpath = "//*[@id=\"homeslider\"]/li[4]/a")
    private WebElement homeSliderContain;

    @FindBy(id = "search_query_top")
    private WebElement searchField;

    @FindBy(xpath = "//*[@id=\"home-page-tabs\"]/li[1]/a")
    private WebElement popular;

    @FindBy(id = "email_create")
    public  WebElement emailFill;

    @FindBy(id = "SubmitCreate")
    public WebElement submitCreate;

    @FindBy(id = "id_gender1")
    public WebElement titleGender;

    @FindBy(id = "customer_firstname")
    private   WebElement firstName;


    @FindBy(id = "customer_lastname")
    private WebElement lastName;

    @FindBy(id = "passwd")
    public WebElement password;

    @FindBy(id = "days")
    public WebElement dayOfBirth;

    @FindBy(id = "months")
    public WebElement monthOfBirth;

    @FindBy(id = "years")
    public WebElement yearOfBirth;

    @FindBy(xpath = "//*[@id=\"account-creation_form\"]/div[1]/div[7]/label")
    public WebElement selectSignUpForOurNewsletter;

    @FindBy(id = "optin")
    public WebElement selectSignUpToReceive;

    @FindBy(id = "firstname")
    public WebElement firstNameYourAddress;

    @FindBy(id = "lastname")
    public WebElement lastNameYourAddress;

    @FindBy(id = "company")
    public WebElement company;

    @FindBy(id = "address1")
    public WebElement address;

    @FindBy(id = "address2")
    public WebElement addressLine2;

    @FindBy(id = "city")
    public WebElement city;

    @FindBy(id = "id_state")
    public WebElement state;

    @FindBy(id = "postcode")
    public WebElement postalCode;

    @FindBy(id = "id_country")
    public WebElement country;

    @FindBy(id = "other")
    public WebElement additionalInformation;

    @FindBy(id = "phone")
    public WebElement homePhone;

    @FindBy(id = "phone_mobile")
    public WebElement mobilePhone;

    @FindBy(id = "alias")
    public WebElement alias;

    @FindBy(id = "submitAccount")
    public WebElement registerButton;


        public void signInButton(){ getLogin().click();}
        public void fillUserMail() { getEmailFill().sendKeys("ion125@gmail.com"); }
        public void clickOnRegisterButton(){ getSubmitCreate().click(); }
        public void clickOnTitle(){ getTitleGender().click();}
        public void fillFirstName(){ getFirstName().sendKeys("Ion");}
        public void fillLastName(){ getLastName().sendKeys("Popescu");}
        public void password(){ getPassword().sendKeys("Abc123.."); }
        //public void clickOnDayOfBirth(){getDayOfBirth().click();}
        public void select15DayOfBirth(){ new Select(dayOfBirth).selectByIndex(15);}
        public void selectJulyMonthOfBirth(){ new Select(monthOfBirth).selectByIndex(7);}
        public void select1980YearOfBirth(){ new Select(yearOfBirth).selectByIndex(42);}
        public void clickSelectSignUpForOurNewsletter(){getSelectSignUpForOurNewsletter().click();}
        public void clickOnReceiveOffers(){ getSelectSignUpToReceive().click();}
        public void fillFirstNameInYourAddress(){
            getFirstName().clear();
            getFirstName().sendKeys("Crist");}
        public void fillLastNameInYourAddress(){
            getLastName().clear();
            getLastName().sendKeys("Georgescu");}

        public void fillcompanyName(){getCompany().sendKeys("Ion's Company");}
        public void fillAddress(){getAddress().sendKeys("Iasi, street Nationala 56");}
        public void fillAddress1(){getAddressLine2().sendKeys("Iasi, street Nicolina 123");}
        public void fillCity() { getCity().sendKeys("Iasi"); }
        public void selectState(){ new Select(state).selectByIndex(11);}
        public void fillPostalCode(){getPostalCode().sendKeys("65788");}
        public void selectCountry(){ new Select(country).selectByIndex(1);}
        public void additionalInformation(){getAdditionalInformation().sendKeys("hello world");}
        public void homePhone(){getHomePhone().sendKeys("0789653546");}
        public void mobilePhone(){getMobilePhone().sendKeys("0789653123");}
        public void assignAnAliasAddress(){
                getAlias().clear();
                getAlias().sendKeys("Bucharest");}
        public void registerButton(){ getRegisterButton().click();}

    //List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
    //Assert.assertTrue("Text not found!", list.size() > 0);
    public void checkAccountCreation(){
        String bodyId = driver.findElement(By.tagName("body")).getAttribute("id");
        Assert.assertEquals("my-account", bodyId);
    }

  //  "Welcome to your account. Here you can manage all of your personal information and orders."








        }










//    public void clickOnWomanButton(){
//        woman.click();
//    }
//    public void clickOnDressButton(){dress.click(); }
//    public void clickOnTshortCategory(){
//        tShort.click();
//    }
//    public void clickOnChar(){
//        cart.click();
//    }
//  //  public void clickOnLogin(){
//     //   login.click();
// //   }
//    public void clickOnContactUs(){
//        contactUs.click();
//    }
//    public }
//    public static void clickOnTitleMr(){ titleGender.click();}














