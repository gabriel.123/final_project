package runner;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
            plugin = {"html:target/cucumberHtmlReport"},
        //pretty:target/customer/
        features = "src\\test\\resources"
)

public class runner {
}
